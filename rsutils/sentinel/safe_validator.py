import datetime
import hashlib
import itertools
import pathlib
import shlex
import subprocess
import typing

import lxml  # type: ignore
import lxml.etree  # type: ignore
import pydantic

from pydantic import BaseModel as BaseSchema


__all__ = ["SAFEManifestValidator"]

# Create a generic variable that can be 'Parent', or any subclass.
T = typing.TypeVar("T", bound="Parent")

NAMESPACES = {
    "safe": "http://www.esa.int/safe/sentinel/1.1",
    "gml": "http://www.opengis.net/gml",
    "xfdu": "urn:ccsds:schema:xfdu:1",
}

DATA_OBJECT_RELATIVE_XPATHS = [
    "/@ID",
    "/byteStream[1]/@mimeType",
    "/byteStream[1]/@size",
    "/byteStream[1]/fileLocation[1]/@href",
    "/byteStream[1]/checksum[1]/@checksumName",
    "/byteStream[1]/checksum[1]/text()",
]


def grouper(
    n: int, iterable: typing.Iterable[typing.Any], padvalue: typing.Any = None
) -> typing.Iterator[typing.Tuple[int]]:
    "grouper(3, 'abcdefg', 'x') --> ('a','b','c'), ('d','e','f'), ('g','x','x')"
    return itertools.zip_longest(*[iter(iterable)] * n, fillvalue=padvalue)


class DataObject(BaseSchema):
    id: str
    type: str
    size: int
    path: pathlib.Path
    abs_path: pathlib.Path
    checksum_algorithm: str
    checksum: str

    @pydantic.validator("checksum_algorithm")
    def _ensure_that_we_can_handle_the_checksum_algorithm(
        cls, checksum_algorithm: str, values: typing.Dict[str, typing.Any]
    ) -> str:
        algorithm_name = checksum_algorithm.lower()
        if algorithm_name not in hashlib.algorithms_guaranteed:
            raise ValueError("Don't know how to handle {algorithm_name}: {values['path']}")
        return algorithm_name

    @pydantic.validator("checksum")
    def _check_checksum(cls, xml_checksum: str, values: typing.Dict[str, typing.Any]) -> str:
        fs_checksum = hashlib.new(name=values["checksum_algorithm"], data=values["abs_path"].read_bytes()).hexdigest()
        if fs_checksum != xml_checksum:
            raise ValueError(f"Wrong checksum for {values['abs_path']}")
        return xml_checksum

    @pydantic.validator("abs_path")
    def _check_if_gdal_can_open_binary_files(
        cls, abs_path: pathlib.Path, values: typing.Dict[str, typing.Any]
    ) -> pathlib.Path:
        if values["type"] != "application/octet-stream":
            return abs_path
        cmd = f"gdalinfo {abs_path}"
        proc = subprocess.run(shlex.split(cmd), check=False, capture_output=True)
        if proc.returncode:
            raise ValueError("GDAL couldn't read {abs_path}")
        return abs_path

    @pydantic.validator("abs_path")
    def _check_if_xmls_are_welformed(cls, abs_path: pathlib.Path, values: typing.Dict[str, typing.Any]) -> pathlib.Path:
        if values["type"] != "application/xml":
            return abs_path
        try:
            lxml.etree.ElementTree(lxml.etree.XML(abs_path.read_bytes()))
        except:
            raise ValueError(f"XML cannot be parsed: {abs_path}")
        else:
            return abs_path


def get_data_objects(root_dir: pathlib.Path, tree: lxml.etree.ElementTree, root_xpath: str) -> typing.List[DataObject]:
    xpaths = [root_xpath + xpath for xpath in DATA_OBJECT_RELATIVE_XPATHS]
    objects = []
    for (id, mime_type, size, path, checksum_algorithm, checksum) in zip(*(tree.xpath(xpath) for xpath in xpaths)):
        obj = DataObject(
            id=id,
            type=mime_type,
            size=size,
            path=path,
            abs_path=root_dir / path,
            checksum_algorithm=checksum_algorithm,
            checksum=checksum,
        )
        objects.append(obj)
    return objects


class SAFEManifestValidator(BaseSchema):

    data_objects: typing.List[DataObject]

    @classmethod
    def from_directory(
        cls: typing.Type[T], root_dir: typing.Union[str, pathlib.Path], manifest_filename: str = "manifest.safe"
    ) -> T:
        root_dir = pathlib.Path(root_dir).expanduser().resolve()
        manifest = root_dir / manifest_filename
        manifest_tree = lxml.etree.ElementTree(lxml.etree.XML(manifest.read_bytes()))
        instance = cls(data_objects=get_data_objects(root_dir, manifest_tree, "//*/dataObject"))
        return instance


# l1c = SAFEManifestValidator.from_directory(
# "tests/data/S2B_MSIL1C_20191218T072159_N0208_R063_T38KMA_20191218T094316.SAFE"
# )
# l2a = SAFEManifestValidator.from_directory(
# "tests/data/S2B_MSIL2A_20191218T003659_N0213_R059_T55MGR_20191218T023503.SAFE"
# )
