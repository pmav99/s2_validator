import pathlib


TEST_DIR = pathlib.Path(__file__).parent.expanduser().resolve()
ROOT_DIR = TEST_DIR.parent
DATA_DIR = TEST_DIR / "data"
