import pytest

from .. import DATA_DIR


L1C_PRODUCTS = pytest.mark.parametrize(
    "product_dir",
    [
        pytest.param(
            DATA_DIR / "S2B_MSIL1C_20191218T072159_N0208_R063_T38KMA_20191218T094316.SAFE",
            id="S2B_MSIL1C_20191218T072159_N0208_R063_T38KMA_20191218T094316.SAFE",
        )
    ],
)

L2A_PRODUCTS = pytest.mark.parametrize(
    "product_dir",
    [
        pytest.param(
            DATA_DIR / "S2B_MSIL2A_20191218T003659_N0213_R059_T55MGR_20191218T023503.SAFE",
            id="S2B_MSIL2A_20191218T003659_N0213_R059_T55MGR_20191218T023503.SAFE",
        )
    ],
)
