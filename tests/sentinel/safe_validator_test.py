import pydantic

from rsutils.sentinel import SAFEManifestValidator

from . import L1C_PRODUCTS, L2A_PRODUCTS


@L1C_PRODUCTS
def test_validate_L1C(product_dir):
    l1c = SAFEManifestValidator.from_directory(product_dir)
    assert isinstance(l1c, pydantic.BaseModel)
    assert isinstance(l1c, SAFEManifestValidator)


@L2A_PRODUCTS
def test_validate_L2A(product_dir):
    l2a = SAFEManifestValidator.from_directory(product_dir)
    assert isinstance(l2a, pydantic.BaseModel)
    assert isinstance(l2a, SAFEManifestValidator)
